#include "stdio.h"
#include "stdio.h"

#include "iomsh.h"
#include "tetgenio.h"

int main(int argc, char const *argv[])
{
    if (argc != 6) {
        printf("Wrong argument list.\n\n");

        printf("The right way is:\n");
        printf("./tetmsh input_base_path tetrmesh_info_path "
               "tetrmesh_coordinate_path tetrmesh_tetrahedron_path "
               "tetrmesh_triangle_path\n\n");

        printf("Example: ./tetmsh my_mesh.1 mesh.msh coordinate.msh "
               "tetrahedron.msh triangle.msh\n\n");

        printf("input_base_path - (input) path to generated by TetGen set of "
               "files like my_mesh.1.node, my_mesh.1.ele, my_mesh.1.edge, "
               "my_mesh.1.face and so on\n");

        printf("tetrmesh_info_path - (output) path to TetrMesh mesh info file\n");
        printf("tetrmesh_coordinate_path - (output) path to TetrMesh coordinates file\n");
        printf("tetrmesh_tetrahedron_path - (output) path to TetrMesh tetrahedron file\n");
        printf("tetrmesh_triangle_path - (output) path to TetrMesh triangle file\n");
        printf("\n");
        return 0;
    }

	char mesh_params_file[50], coordinates_file[50], tetrahedron_file[50],
        triangle_file[50];
    int status;

    tetgenio io;
    char fname[20];

    sprintf(fname, argv[1]);
    io.load_node(fname);
    io.load_face(fname);
    io.load_tet(fname);

	sprintf (mesh_params_file, argv[2]);
	sprintf (coordinates_file, argv[3]);
	sprintf (tetrahedron_file, argv[4]);
	sprintf (triangle_file, argv[5]);

	status = WriteMesh_MSH(mesh_params_file, io.numberofpoints,
		io.numberoftetrahedra, io.numberoftrifaces);

	if (status == -1) {
		printf("mesh file write error\n");
		return 1;
	}

	status = WriteCoordinate_MSH(coordinates_file, io.numberofpoints,
		io.pointlist);

	if (status == -1) {
		printf("coordinates file write error\n");
		return 1;
	}

	status = WriteTetrahedron_MSH(tetrahedron_file, io.numberoftetrahedra,
		io.tetrahedronlist);

	if (status == -1) {
		printf("tetrahedron file write error\n");
		return 1;
	}

    int *tri_types = (int *)malloc(io.numberoftrifaces * sizeof(int));
    memset(tri_types, 0, io.numberoftrifaces);

	status = WriteTriangle_MSH(triangle_file, io.numberoftrifaces,
		io.trifacelist, tri_types);

	if (status == -1) {
		printf("triangles file write error\n");
		return 1;
	}    
	return 0;
}