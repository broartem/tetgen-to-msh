CC = mpic++
CCFLAGS = 

MESH_LIB_DIR = ./lib/TetrMesh/lib
MESH_INC_DIR = ./lib/TetrMesh/include

INCOPT = -I. -I$(MESH_INC_DIR)
LIBOPT = -L$(MESH_LIB_DIR) -ltetrmesh

OBJ = tetgenio.o main.o

.SUFFIXES: .cxx .o

tetmsh: $(OBJ)
	$(CC) $(OBJ) $(LIBOPT) -o tetmsh

.cxx.o :
	$(CC) $(CCFLAGS) $(INCOPT) -c $< -o $@

clean:
	rm -f *.o *.d tetmsh

