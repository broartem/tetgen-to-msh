# Tetgen (.node, .ele, .face) to TetrMesh format converter

## Installation

```
cd /path/to/source/lib/TetrMesh
make
cd /path/to/source
make
```

## Running

```
./tetmsh /path/to/sourcedir/tetgen-filename-without-extension /path/to/destination/folder
```

## Examples

Take files `./data/input.node`,  `./data/input.ele`,  `./data/input.face` and save new files
into `./data` directory 

```
./tetmsh ./data/input ./data
```