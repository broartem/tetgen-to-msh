#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "tomesh_to_csr_graph.h"

static void wsort1(int *A, int l, int r);
static void wsort1(int *A, int l, int r)
{
  int tmp;
  int B = A[(l + r) / 2];
  int i = l, j = r;

  while( i <= j )
  {    
	while( A[i] < B ) i++;
    while( A[j] > B ) j--;
    if( i <= j )
    {
      tmp = A[i];
      A[i] = A[j];
      A[j] = tmp;
      i++;
      j--;   
    }   
  }
  
  if( l < j ) wsort1(A, l, j);
  if( i < r ) wsort1(A, i, r);
}

int TOmesh_to_CSRGraph(int Np, int Nt, int **T, int No, int **O, int **xadj, int **adjncy, int lite)
{
	
	int *xxadj = NULL, *xadjncy = NULL;

	xxadj = (int *)malloc((Np+1) * sizeof(int)); if(!xxadj) return 1;
	memset(xxadj, 0, (Np+1)*sizeof(int));

	{
		int i, j, k;

		for(i=0; i<Nt; i++)
			xxadj[T[i][0] + 1] += 3,
			xxadj[T[i][1] + 1] += 3,
			xxadj[T[i][2] + 1] += 3,
			xxadj[T[i][3] + 1] += 3;

		for(i=0; i<No; i++)
			xxadj[O[i][0] + 1] += 4,
			xxadj[O[i][1] + 1] += 4,
			xxadj[O[i][2] + 1] += 4,
			xxadj[O[i][3] + 1] += 4,
			xxadj[O[i][4] + 1] += 4,
			xxadj[O[i][5] + 1] += 4;

		for(i=2; i<=Np; i++)
			xxadj[i] = xxadj[i-1] + xxadj[i];

		xadjncy = (int *)malloc(xxadj[Np] * sizeof(int)); if(!xadjncy) return 1;
		memset(xadjncy, 0, xxadj[Np]*sizeof(int));

		for(i=0; i<Nt; i++)
		{
			xadjncy[xxadj[T[i][0]] + 0] = T[i][1];
			xadjncy[xxadj[T[i][0]] + 1] = T[i][2];
			xadjncy[xxadj[T[i][0]] + 2] = T[i][3];
			xxadj[T[i][0]] += 3;

			xadjncy[xxadj[T[i][1]] + 0] = T[i][0];
			xadjncy[xxadj[T[i][1]] + 1] = T[i][2];
			xadjncy[xxadj[T[i][1]] + 2] = T[i][3];
			xxadj[T[i][1]] += 3;

			xadjncy[xxadj[T[i][2]] + 0] = T[i][0];
			xadjncy[xxadj[T[i][2]] + 1] = T[i][1];
			xadjncy[xxadj[T[i][2]] + 2] = T[i][3];
			xxadj[T[i][2]] += 3;

			xadjncy[xxadj[T[i][3]] + 0] = T[i][0];
			xadjncy[xxadj[T[i][3]] + 1] = T[i][1];
			xadjncy[xxadj[T[i][3]] + 2] = T[i][2];
			xxadj[T[i][3]] += 3;			
		} // for i

		for(i=0; i<No; i++)
		{
			xadjncy[xxadj[O[i][0]] + 0] = O[i][1];
			xadjncy[xxadj[O[i][0]] + 1] = O[i][2];
			xadjncy[xxadj[O[i][0]] + 2] = O[i][3];
			xadjncy[xxadj[O[i][0]] + 3] = O[i][4];
			xxadj[O[i][0]] += 4;

			xadjncy[xxadj[O[i][1]] + 0] = O[i][0];
			xadjncy[xxadj[O[i][1]] + 1] = O[i][2];
			xadjncy[xxadj[O[i][1]] + 2] = O[i][4];
			xadjncy[xxadj[O[i][1]] + 3] = O[i][5];
			xxadj[O[i][1]] += 4;

			xadjncy[xxadj[O[i][2]] + 0] = O[i][0];
			xadjncy[xxadj[O[i][2]] + 1] = O[i][1];
			xadjncy[xxadj[O[i][2]] + 2] = O[i][3];
			xadjncy[xxadj[O[i][2]] + 3] = O[i][5];
			xxadj[O[i][2]] += 4;

			xadjncy[xxadj[O[i][3]] + 0] = O[i][0];
			xadjncy[xxadj[O[i][3]] + 1] = O[i][2];
			xadjncy[xxadj[O[i][3]] + 2] = O[i][4];
			xadjncy[xxadj[O[i][3]] + 3] = O[i][5];
			xxadj[O[i][3]] += 4;

			xadjncy[xxadj[O[i][4]] + 0] = O[i][0];
			xadjncy[xxadj[O[i][4]] + 1] = O[i][1];
			xadjncy[xxadj[O[i][4]] + 2] = O[i][3];
			xadjncy[xxadj[O[i][4]] + 3] = O[i][5];
			xxadj[O[i][4]] += 4;
			
			xadjncy[xxadj[O[i][5]] + 0] = O[i][1];
			xadjncy[xxadj[O[i][5]] + 1] = O[i][2];
			xadjncy[xxadj[O[i][5]] + 2] = O[i][3];
			xadjncy[xxadj[O[i][5]] + 3] = O[i][4];
			xxadj[O[i][5]] += 4;			
		} // for i

		for(i=Np; i>0; i--) xxadj[i] = xxadj[i-1]; xxadj[0] = 0;
		for(i=0; i<Np; i++)	wsort1(&xadjncy[xxadj[i]], 0, xxadj[i+1]-xxadj[i]-1);

		for(i=0; i<Np; i++)
		{

			for(j=xxadj[i]+1; j<xxadj[i+1]; j++)
			{
				if(xadjncy[j] == xadjncy[j-1]) xadjncy[j-1] = (-1);
			} // for j

			if(lite == 1)
			for(j=xxadj[i]; j<xxadj[i+1]; j++)
			{
				if(xadjncy[j] < i) xadjncy[j] = (-1);
			} // for j

		} // for i

		for(i=0, k=0, j=0; i<Np; i++)
		{
			for( ; j<xxadj[i+1]; j++)
			{
				if(xadjncy[j] != (-1))
				{
					xadjncy[k] = xadjncy[j];
					k += 1;
				} // if
			} // for j
			xxadj[i+1] = k;
		} // for i
	}

	{
		int *tmp = NULL;
		tmp = (int *)realloc(xadjncy, xxadj[Np]*sizeof(int));
		xadjncy = tmp;
	}

	(*xadj) = xxadj;
	(*adjncy) = xadjncy;

	return 0;
}

