#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "iodmsh.h"

// ������ ����� � ������ ����������� �����, ���������� � �������������� ��������� �������
// �� ���� ������������ ������ ��� ����� � ��������� � ����������� �����, �����������
// � �������������� �������.
int WriteMesh_DMT(char *source, t_DistrMsh DM)
{
	FILE *file = NULL;
	int i;

	file = fopen(source, "w");
	if(!file) { fprintf(stderr, "Can't create file %s.\n", source); return 1; }

	fprintf(file, "%d %d %d\n", DM.Np, DM.Nt, DM.Ntr);

	fprintf(file, "%d ", DM.Nfc);
	for(i=0; i<DM.Nfc; i++) fprintf(file, "%d ", DM.PT[i]); fprintf(file, "%d\n", DM.PT[DM.Nfc]);

	fprintf(file, "%d ", DM.Nft);
	for(i=0; i<DM.Nft; i++) fprintf(file, "%d ", DM.TT[i]); fprintf(file, "%d\n", DM.TT[DM.Nft]);

	fclose(file);

	return 0;
}

// ������ ����� � ������ ����������� �����, ���������� � �������������� �������� �������
// �� ���� ������������ ������ ��� ����� � ��������� � ����������� �����, �����������
// � �������������� �������.
int WriteMesh_DMB(char *source, t_DistrMsh DM)
{
	FILE *file = NULL;
	int i;

	file = fopen(source, "w");
	if(!file) { fprintf(stderr, "Can't create file %s.\n", source); return 1; }

	fprintf(file, "%d %d %d\n", DM.Np, DM.Nt, DM.Ntr);

	fprintf(file, "%d ", DM.Nfc);
	for(i=0; i<DM.Nfc; i++) fprintf(file, "%d ", DM.PT[i]); fprintf(file, "%d\n", DM.PT[DM.Nfc]);

	fprintf(file, "%d ", DM.Nft);
	for(i=0; i<DM.Nft; i++) fprintf(file, "%d ", DM.TT[i]); fprintf(file, "%d\n", DM.TT[DM.Nft]);

	fclose(file);

	return 0;
}

// ������ ����� � ������ ���������� �����, ���������� � �������������� ��������� �������
// �� ���� ������������ ���������� ��� ����� � ��������� �� ����������, � ������� �����
// ������� �������� ������������� ������ �� ������.
int ReadMesh_DMT(char *source, t_DistrMsh *DM)
{
	FILE *file = NULL;
	int i;

	file = fopen(source, "r");
	if(!file) { fprintf(stderr, "Can't open file %s.\n", source); return 1; }

	if(fscanf(file, "%d %d %d", &(DM->Np), &(DM->Nt), &(DM->Ntr)) != 3){ fprintf(stderr, "Can't read from file.\n"); return 1; }

	if(fscanf(file, "%d", &(DM->Nfc)) != 1){ fprintf(stderr, "Can't read from file.\n"); return 1; }
	DM->PT = NULL; DM->PT = (int *)malloc((DM->Nfc + 1) * sizeof(int)); if(!DM->PT) { fprintf(stderr, "Can't allocate memory.\n"); return 1; }
	for(i=0; i<=DM->Nfc; i++)
		if(fscanf(file, "%d", &(DM->PT[i])) != 1){ fprintf(stderr, "Can't read from file.\n"); return 1; }

	if(fscanf(file, "%d", &(DM->Nft)) != 1){ fprintf(stderr, "Can't read from file.\n"); return 1; }
	DM->TT = NULL; DM->TT = (int *)malloc((DM->Nft + 1) * sizeof(int)); if(!DM->TT) { fprintf(stderr, "Can't allocate memory.\n"); return 1; }
	for(i=0; i<=DM->Nft; i++)
		if(fscanf(file, "%d", &(DM->TT[i])) != 1){ fprintf(stderr, "Can't read from file.\n"); return 1; }

	fclose(file);
	
	return 0;
}

// ������ ����� � ������ ���������� �����, ���������� � �������������� �������� �������
// �� ���� ������������ ���������� ��� ����� � ��������� �� ����������, � ������� �����
// ������� �������� ������������� ������ �� ������.
int ReadMesh_DMB(char *source, t_DistrMsh *DM)
{
	FILE *file = NULL;
	int i;

	file = fopen(source, "r");
	if(!file) { fprintf(stderr, "Can't open file %s.\n", source); return 1; }

	if(fscanf(file, "%d %d %d", &(DM->Np), &(DM->Nt), &(DM->Ntr)) != 3){ fprintf(stderr, "Can't read from file.\n"); return 1; }

	if(fscanf(file, "%d", &(DM->Nfc)) != 1){ fprintf(stderr, "Can't read from file.\n"); return 1; }
	DM->PT = NULL; DM->PT = (int *)malloc((DM->Nfc + 1) * sizeof(int)); if(!DM->PT) { fprintf(stderr, "Can't allocate memory.\n"); return 1; }
	for(i=0; i<=DM->Nfc; i++)
		if(fscanf(file, "%d", &(DM->PT[i])) != 1){ fprintf(stderr, "Can't read from file.\n"); return 1; }

	if(fscanf(file, "%d", &(DM->Nft)) != 1){ fprintf(stderr, "Can't read from file.\n"); return 1; }
	DM->TT = NULL; DM->TT = (int *)malloc((DM->Nft + 1) * sizeof(int)); if(!DM->TT) { fprintf(stderr, "Can't allocate memory.\n"); return 1; }
	for(i=0; i<=DM->Nft; i++)
		if(fscanf(file, "%d", &(DM->TT[i])) != 1){ fprintf(stderr, "Can't read from file.\n"); return 1; }

	fclose(file);
	
	return 0;
}

// ������ ����� ��������� ����� �����, ���������� � �������������� �������� �������
// source_mask - "�����" ���� ������ � ������������ �����. ��� ������ ������ ���
// ������������ ������ ����� � ����� ����������� ��� ����� � ������� "%d".
// left, right - �������� ��������������� ����������� �����: [left, right).
// DM - ��������� � �������� ������������ ������ �� ������.
// C - ��������� �� ������, � ������� ����� ����������� ������. ������ ��� ������
// ������ ���� ��� �������������� �������� ������������� ��������������. �
// ���������� ������ ������������ ���������� ���� � ��������������� I,
// ������������� [left, right] �� ���� X, Y � Z, ��������������, ����� �������� �
// �������� ������� C[(I-left)*3 + 0], C[(I-left)*3 + 1], C[(I-left)*3 + 2].
int ReadCoordinate_DMT(char *source_mask, int left, int right, t_DistrMsh DM, double *C)
{
	FILE *file;
	char FILE_NAME[257]; // ����� ��� �������� ����� ��������������� �����
	int pos; // ������� ������� � ������� ��� ���������� ������

	int *TABLE = NULL; // ������� � ����������� ����������� �� ������ ������
	// � ������� ��� ������� ����� ������ ���������� ������� � ������ �������� �
	// �������� �������� ������� ��������� ???
	// !!! ��������, �� ������ �������� �� ������� ��������� !!!

	int i, j, i1, i2;

	// ��������� ������ � ��������� ������� ������ ������
	TABLE = (int *)malloc(DM.Nfc * 2 * sizeof(int)); if(!TABLE) { fprintf(stderr, "Can't allocate memory.\n"); return 1; }
	memset(TABLE, 0, DM.Nfc * 2 * sizeof(int));

	// ���������� ������� ������ ������
	for(i=0; i<DM.Nfc; i++)
		if( (left >= DM.PT[i]) && (left < DM.PT[i+1]) )
			break;

	if(i == DM.Nfc) { fprintf(stderr, "Wrong interval of nodes: [%d, %d)\n", left, right); return 1; }

	TABLE[i*2 + 0] = left - DM.PT[i];
	i1 = i;

	for( ; i<DM.Nfc; i++)
		if( (right > DM.PT[i]) && (right <= DM.PT[i+1]) )
			break;

	if(i == DM.Nfc) { fprintf(stderr, "Wrong interval of nodes: [%d, %d)\n", left, right); return 1; }

	TABLE[i*2 + 1] = right - DM.PT[i];
	i2 = i;

	if(i1 != i2)
	{
		TABLE[i1*2 + 1] = DM.PT[i1+1] - DM.PT[i1];
		for(i=i1+1; i<i2; i++)
			TABLE[i*2 + 0] = 0,
			TABLE[i*2 + 1] = DM.PT[i+1] - DM.PT[i];
		TABLE[i2*2 + 0] = 0;
	} // if i1 != i2

#if 0
	printf("TABLE:\n");
	for(i=0; i<DM.Nfc; i++)
		printf("[%d ; %d)\n", TABLE[i*2 + 0], TABLE[i*2 + 1]);
#endif

	// ��������������� ������ ������
	// ������ ����������� � ������, ������ ��� �������� ���� �������������� �������� �������������
	for(i=0, pos=0; i<DM.Nfc; i++)
		if(TABLE[i*2 + 1] != 0)
		{
			char line[257], *oldstr, *newstr;

			sprintf(FILE_NAME, "%s%d.dmt", source_mask, i);
			file = NULL; file = fopen(FILE_NAME, "r"); if(!file) { fprintf(stderr, "Can't open file %s.\n", FILE_NAME); return 1; }

			for(j=0; j<TABLE[i*2 + 0]; j++)
				if(!fgets(line, 257, file)){ fprintf(stderr, "Can't read line.\n"); return 1; }

			for( ; j<TABLE[i*2 + 1]; j++)
			{
				oldstr = line; newstr = NULL;
				if(!fgets(line, 257, file)){ fprintf(stderr, "Can't read line.\n"); return 1; }
				C[pos*3 + 0] = strtod(oldstr, &newstr); oldstr = newstr;
				C[pos*3 + 1] = strtod(oldstr, &newstr); oldstr = newstr;
				C[pos*3 + 2] = strtod(oldstr, &newstr);
				pos += 1;
			} // for j

			fclose(file);
		} // if

	free(TABLE);

	return 0;
}

// ������ ����� ��������� ����� �����, ���������� � �������������� �������� �������
// source_mask - "�����" ���� ������ � ������������ �����. ��� ������ ������ ���
// ������������ ������ ����� � ����� ����������� ��� ����� � ������� "%d".
// left, right - �������� ��������������� ����������� �����: [left, right).
// DM - ��������� � �������� ������������ ������ �� ������.
// C - ��������� �� ������, � ������� ����� ����������� ������. ������ ��� ������
// ������ ���� ��� �������������� �������� ������������� ��������������. �
// ���������� ������ ������������ ���������� ���� � ��������������� I,
// ������������� [left, right] �� ���� X, Y � Z, ��������������, ����� �������� �
// �������� ������� C[(I-left)*3 + 0], C[(I-left)*3 + 1], C[(I-left)*3 + 2].
int ReadCoordinate_DMB(char *source_mask, int left, int right, t_DistrMsh DM, double *C)
{
	FILE *file;
	char FILE_NAME[257]; // ����� ��� �������� ����� ��������������� �����
	int get; // ���������� ��� ������ ����� ����������� ������
	int pos = 0; // ������� ������� � ������� ��� ���������� ������

	int *TABLE = NULL; // ������� � ����������� ����������� �� ������ ������
	// � ������� ��� ������� ����� ������ ���������� ������� � ������ �������� �
	// �������� �������� ������� ���������

	int i, i1, i2;

	// ��������� ������ � ��������� ������� ������ ������
	TABLE = (int *)malloc(DM.Nfc * 2 * sizeof(int)); if(!TABLE) { fprintf(stderr, "Can't allocate memory.\n"); return 1; }
	memset(TABLE, 0, DM.Nfc * 2 * sizeof(int));

	// ���������� ������� ������ ������
	for(i=0; i<DM.Nfc; i++)
		if( (left >= DM.PT[i]) && (left < DM.PT[i+1]) )
			break;

	if(i == DM.Nfc) { fprintf(stderr, "Wrong interval of nodes: [%d, %d)\n", left, right); return 1; }

	TABLE[i*2 + 0] = left - DM.PT[i];
	i1 = i;

	for( ; i<DM.Nfc; i++)
		if( (right > DM.PT[i]) && (right <= DM.PT[i+1]) )
			break;

	if(i == DM.Nfc) { fprintf(stderr, "Wrong interval of nodes: [%d, %d)\n", left, right); return 1; }

	TABLE[i*2 + 1] = right - DM.PT[i];
	i2 = i;

	if(i1 != i2)
	{
		TABLE[i1*2 + 1] = DM.PT[i1+1] - DM.PT[i1];
		for(i=i1+1; i<i2; i++)
			TABLE[i*2 + 0] = 0,
			TABLE[i*2 + 1] = DM.PT[i+1] - DM.PT[i];
		TABLE[i2*2 + 0] = 0;
	} // if i1 != i2

#if 0
	printf("TABLE:\n");
	for(i=0; i<DM.Nfc; i++)
		printf("[%d ; %d)\n", TABLE[i*2 + 0], TABLE[i*2 + 1]);
#endif

	// ��������������� ������ ������
	// ������ ����������� � ������, ������ ��� �������� ���� �������������� �������� �������������
	for(i=0; i<DM.Nfc; i++)
		if(TABLE[i*2 + 1] != 0)
		{
			sprintf(FILE_NAME, "%s%d.dmb", source_mask, i);
			file = NULL; file = fopen(FILE_NAME, "rb"); if(!file) { fprintf(stderr, "Can't open file %s.\n", FILE_NAME); return 1; }

			if(TABLE[i*2 + 0] != 0) fseek(file, TABLE[i*2 + 0] * 3 * sizeof(double), SEEK_SET);
			
			get = (int)fread(&C[pos], sizeof(double), 3 * (TABLE[i*2 + 1] - TABLE[i*2 + 0]), file);
			if(get != 3 * (TABLE[i*2 + 1] - TABLE[i*2 + 0]) ) { fclose(file); fprintf(stderr, "Can't read data from file %s.\n", FILE_NAME); return 1; }
			pos += get;
            
			fclose(file);
		} // if

	free(TABLE);

	return 0;
}

// ������ ����� ���������� �����, ���������� � �������������� ��������� �������
// source_mask - "�����" ���� ������ � �����������. ��� ������ ������ ���
// ������������ ������ ����� � ����� ����������� ��� ����� � ������� "%d".
// left, right - �������� "�������" ����������� ����������: [left, right).
// DM - ��������� � �������� ������������ ������ �� ������.
// T - ��������� �� ������ � ��������� ���������� �����. ������ ��� ������
// ������ �������������� ������ ���� �������� ������������� ��������������.
// � ���������� ���������� ������ �������� � ������� I, �������������
// ������� [left, right], ����� ����������� ���������� �������
// T[(I-left)*4 + 0], T[(I-left)*4 + 2], T[(I-left)*4 + 2], T[(I-left)*4 + 3].
int ReadTetrahedron_DMT(char *source_mask, int left, int right, t_DistrMsh DM, int *T)
{
	FILE *file;
	char FILE_NAME[257]; // ����� ��� �������� ����� ��������������� �����
	int pos; // ������� ������� � ������� ��� ���������� ������

	int *TABLE = NULL; // ������� � ����������� ����������� �� ������ ������
	// � ������� ��� ������� ����� ������ ���������� ������� � ������ �������� �
	// �������� �������� ������� ���������

	int i, j, i1, i2;

	// ��������� ������ � ��������� ������� ������ ������
	TABLE = (int *)malloc(DM.Nft * 2 * sizeof(int)); if(!TABLE) { fprintf(stderr, "Can't allocate memory.\n"); return 1; }
	memset(TABLE, 0, DM.Nft * 2 * sizeof(int));

	// ���������� ������� ������ ������
	for(i=0; i<DM.Nft; i++)
		if( (left >= DM.TT[i]) && (left < DM.TT[i+1]) )
			break;

	if(i == DM.Nft) { fprintf(stderr, "Wrong interval of nodes: [%d, %d)\n", left, right); return 1; }

	TABLE[i*2 + 0] = left - DM.TT[i];
	i1 = i;

	for( ; i<DM.Nft; i++)
		if( (right > DM.TT[i]) && (right <= DM.TT[i+1]) )
			break;

	if(i == DM.Nft) { fprintf(stderr, "Wrong interval of nodes: [%d, %d)\n", left, right); return 1; }

	TABLE[i*2 + 1] = right - DM.TT[i];
	i2 = i;

	if(i1 != i2)
	{
		TABLE[i1*2 + 1] = DM.TT[i1+1] - DM.TT[i1];
		for(i=i1+1; i<i2; i++)
			TABLE[i*2 + 0] = 0,
			TABLE[i*2 + 1] = DM.TT[i+1] - DM.TT[i];
		TABLE[i2*2 + 0] = 0;
	} // if i1 != i2

#if 0
	printf("TABLE:\n");
	for(i=0; i<DM.Nft; i++)
		printf("[%d ; %d)\n", TABLE[i*2 + 0], TABLE[i*2 + 1]);
#endif

	// ��������������� ������ ������
	// ������ ����������� � ������, ������ ��� �������� ���� �������������� �������� �������������
	for(i=0, pos=0; i<DM.Nft; i++)
		if(TABLE[i*2 + 1] != 0)
		{
			char line[257], *oldstr, *newstr;

			sprintf(FILE_NAME, "%s%d.dmt", source_mask, i);
			file = NULL; file = fopen(FILE_NAME, "r"); if(!file) { fprintf(stderr, "Can't open file %s.\n", FILE_NAME); return 1; }

			for(j=0; j<TABLE[i*2 + 0]; j++)
				if(!fgets(line, 257, file)){ fprintf(stderr, "Can't read line.\n"); return 1; }

			for( ; j<TABLE[i*2 + 1]; j++)
			{
				oldstr = line; newstr = NULL;
				if(!fgets(line, 257, file)){ fprintf(stderr, "Can't read line.\n"); return 1; }
				T[pos*4 + 0] = (int)strtol(oldstr, &newstr, 10); oldstr = newstr;
				T[pos*4 + 1] = (int)strtol(oldstr, &newstr, 10); oldstr = newstr;
				T[pos*4 + 2] = (int)strtol(oldstr, &newstr, 10); oldstr = newstr;
				T[pos*4 + 3] = (int)strtol(oldstr, &newstr, 10);
				pos += 1;
			} // for j

			fclose(file);
		} // if

	free(TABLE);

	return 0;
}

// ������ ����� ���������� �����, ���������� � �������������� �������� �������
// source_mask - "�����" ���� ������ � �����������. ��� ������ ������ ���
// ������������ ������ ����� � ����� ����������� ��� ����� � ������� "%d".
// left, right - �������� "�������" ����������� ����������: [left, right).
// DM - ��������� � �������� ������������ ������ �� ������.
// T - ��������� �� ������ � ��������� ���������� �����. ������ ��� ������
// ������ �������������� ������ ���� �������� ������������� ��������������.
// � ���������� ���������� ������ �������� � ������� I, �������������
// ������� [left, right], ����� ����������� ���������� �������
// T[(I-left)*4 + 0], T[(I-left)*4 + 2], T[(I-left)*4 + 2], T[(I-left)*4 + 3].
int ReadTetrahedron_DMB(char *source_mask, int left, int right, t_DistrMsh DM, int *T)
{
	FILE *file;
	char FILE_NAME[257]; // ����� ��� �������� ����� ��������������� �����
	int get; // ���������� ��� ������ ����� ����������� ������
	int pos = 0; // ������� ������� � ������� ��� ���������� ������

	int *TABLE = NULL; // ������� � ����������� ����������� �� ������ ������
	// � ������� ��� ������� ����� ������ ���������� ������� � ������ �������� �
	// �������� �������� ������� ���������

	int i, i1, i2;

	// ��������� ������ � ��������� ������� ������ ������
	TABLE = (int *)malloc(DM.Nft * 2 * sizeof(int)); if(!TABLE) { fprintf(stderr, "Can't allocate memory.\n"); return 1; }
	memset(TABLE, 0, DM.Nft * 2 * sizeof(int));

	// ���������� ������� ������ ������
	for(i=0; i<DM.Nft; i++)
		if( (left >= DM.TT[i]) && (left < DM.TT[i+1]) )
			break;

	if(i == DM.Nft) { fprintf(stderr, "Wrong interval of nodes: [%d, %d)\n", left, right); return 1; }

	TABLE[i*2 + 0] = left - DM.TT[i];
	i1 = i;

	for( ; i<DM.Nft; i++)
		if( (right > DM.TT[i]) && (right <= DM.TT[i+1]) )
			break;

	if(i == DM.Nft) { fprintf(stderr, "Wrong interval of nodes: [%d, %d)\n", left, right); return 1; }

	TABLE[i*2 + 1] = right - DM.TT[i];
	i2 = i;

	if(i1 != i2)
	{
		TABLE[i1*2 + 1] = DM.TT[i1+1] - DM.TT[i1];
		for(i=i1+1; i<i2; i++)
			TABLE[i*2 + 0] = 0,
			TABLE[i*2 + 1] = DM.TT[i+1] - DM.TT[i];
		TABLE[i2*2 + 0] = 0;
	} // if i1 != i2

#if 0
	printf("TABLE:\n");
	for(i=0; i<DM.Nft; i++)
		printf("[%d ; %d)\n", TABLE[i*2 + 0], TABLE[i*2 + 1]);
#endif

	// ��������������� ������ ������
	// ������ ����������� � ������, ������ ��� �������� ���� �������������� �������� �������������
	for(i=0; i<DM.Nft; i++)
		if(TABLE[i*2 + 1] != 0)
		{
			sprintf(FILE_NAME, "%s%d.dmb", source_mask, i);
			file = NULL; file = fopen(FILE_NAME, "rb"); if(!file) { fprintf(stderr, "Can't open file %s.\n", FILE_NAME); return 1; }
			
			if(TABLE[i*2 + 0] != 0) fseek(file, TABLE[i*2 + 0] * 4 * sizeof(int), SEEK_SET);
			
			get = (int)fread(&T[pos], sizeof(int), 4 * (TABLE[i*2 + 1] - TABLE[i*2 + 0]), file);
			if(get != 4 * (TABLE[i*2 + 1] - TABLE[i*2 + 0]) ) { fclose(file); fprintf(stderr, "Can't read data from file %s.\n", FILE_NAME); return 1; }
			pos += get;		

			fclose(file);
		} // if

	free(TABLE);

	return 0;
}

// ������ ����� ������������� ������������� �����, ���������� � �������������� ��������� �������
// source - ��� ����� � ��������� ������������� �����
// left, right - �������� "�������" ����������� �������������: [left, right).
// Tr - ��������� �� ������ � ��������� ������������� ������������� �����.
// TrType - ��������� �� ������ ��� ������ ��������������� ���������
// ������������, ��������������� �������������. ������ ��� ������
// ������� �������������� ������ ���� �������� ������������� ��������������.
// � ���������� ���������� ������ ������������� ����������� � ������� I,
// ������������� ������� [left, right], ����� ����������� ���������� ��������
// Tr[(I-left)*3 + 0], Tr[(I-left)*3 + 1], Tr[(I-left)*3 + 2], TrType[I - left].
int ReadTriangle_DMT(char *source, int left, int right, int *Tr, int *TrType)
{
	FILE *file = NULL;
	char line[257], *oldstr, *newstr;
	int i;

	file = fopen(source, "r");
	if(!file) { fprintf(stderr, "Can't open file %s.\n", source); return 1; }

	for(i=0; i<left; i++)
		if(!fgets(line, 257, file)){ fprintf(stderr, "Can't read line.\n"); return 1; }

	for(i=0; i<right-left; i++)
	{
		oldstr = line; newstr = NULL;
		if(!fgets(line, 257, file)){ fprintf(stderr, "Can't read line.\n"); return 1; }
		Tr[i*3 + 0] = (int)strtol(oldstr, &newstr, 10); oldstr = newstr;
		Tr[i*3 + 1] = (int)strtol(oldstr, &newstr, 10); oldstr = newstr;
		Tr[i*3 + 2] = (int)strtol(oldstr, &newstr, 10); oldstr = newstr;
		TrType[i]   = (int)strtol(oldstr, &newstr, 10);
	}
	
	fclose(file);

	return 0;
}

// ������ ����� ������������� ������������� �����, ���������� � �������������� �������� �������
// source - ��� ����� � ��������� ������������� �����
// left, right - �������� "�������" ����������� �������������: [left, right).
// Tr - ��������� �� ������ � ��������� ������������� ������������� �����.
// TrType - ��������� �� ������ ��� ������ ��������������� ���������
// ������������, ��������������� �������������. ������ ��� ������
// ������� �������������� ������ ���� �������� ������������� ��������������.
// � ���������� ���������� ������ ������������� ����������� � ������� I,
// ������������� ������� [left, right], ����� ����������� ���������� ��������
// Tr[(I-left)*3 + 0], Tr[(I-left)*3 + 1], Tr[(I-left)*3 + 2], TrType[I - left].
int ReadTriangle_DMB(char *source, int left, int right, int *Tr, int *TrType)
{
	FILE *file = NULL;
	int i, get;

	file = fopen(source, "rb");
	if(!file) { fprintf(stderr, "Can't open file %s.\n", source); return 1; }

	if(left != 0) fseek(file, 4 * left * sizeof(int), SEEK_SET);

	for(i=0; i<right-left; i++)
	{
		get = fread((void *)(&(Tr[i*3])), sizeof(int), 3, file);
		if(get != 3) { fprintf(stderr, "Can't read data from file %s.\n", source); fclose(file); return 1; }
		get = fread((void *)(&(TrType[i])), sizeof(int), 1, file);
		if(get != 1) { fprintf(stderr, "Can't read data from file %s.\n", source); fclose(file); return 1; }
	}
	
	fclose(file);

	return 0;
}

// ������������ ������, ���������� ��� ��������� � �������� ������������ ������
// � �������� ��������� ������� ���������� ��������� �� ��������������� ����������.
int FreeDistrMsh(t_DistrMsh *DM)
{
	DM->Nfc = 0;
	DM->Nft = 0;
	DM->Np = 0;
	DM->Nt = 0;
	DM->Ntr = 0;

	if(DM->PT) free(DM->PT);
	if(DM->TT) free(DM->TT);

	return 0;
}
