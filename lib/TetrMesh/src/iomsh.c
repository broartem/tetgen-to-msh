#include <stdio.h>
#include <stdlib.h>

#include "iomsh.h"

#define MAX_LINE 257

int ReadMesh_MSH(char *source, int *Np, int *Nt, int *Ntr)
{
	FILE *file = NULL;

	file = fopen(source, "r");
	if(!file) { fprintf(stderr, "Can't open file %s.\n", source); return 1; }

	if(fscanf(file, "%d %d %d", Np, Nt, Ntr) != 3){ fprintf(stderr, "Can't read from file.\n"); return 1; }

	fclose(file);

	return 0;
}

int WriteMesh_MSH(char *source, int Np, int Nt, int Ntr)
{
	FILE *file = NULL;

	file = fopen(source, "w");
	if(!file) { fprintf(stderr, "Can't create file %s.\n", source); return 1; }

	fprintf(file, "%d\n%d\n%d\n", Np, Nt, Ntr);

	fclose(file);

	return 0;
}

int ReadCoordinate_MSH(char *source, int Np, double *C)
{
	FILE *file = NULL;
	char line[MAX_LINE], *oldstr, *newstr;
	int i;

	file = fopen(source, "r");
	if(!file) { fprintf(stderr, "Can't open file %s.\n", source); return 1; }

	for(i=0; i<Np; i++)
	{
		oldstr = line; newstr = NULL;
		if(!fgets(line, MAX_LINE, file)){ fprintf(stderr, "Can't read line.\n"); return 1; }
		C[i*3 + 0] = strtod(oldstr, &newstr); oldstr = newstr;
		C[i*3 + 1] = strtod(oldstr, &newstr); oldstr = newstr;
		C[i*3 + 2] = strtod(oldstr, &newstr);
	} // for i

	fclose(file);

	return 0;
}

int WriteCoordinate_MSH(char *source, int Np, double *C)
{
	FILE *file = NULL;
	int i;

	file = fopen(source, "w");
	if(!file) { fprintf(stderr, "Can't create file %s.\n", source); return 1; }

	for(i=0; i<Np; i++)
		fprintf(file, "%.9lf %.9lf %.9lf\n", C[i*3 + 0], C[i*3 + 1], C[i*3 + 2]);
	
	fclose(file);

	return 0;
}

int ReadTetrahedron_MSH(char *source, int Nt, int *T)
{
	FILE *file = NULL;
	char line[MAX_LINE], *oldstr, *newstr;
	int i;

	file = fopen(source, "r");
	if(!file) { fprintf(stderr, "Can't open file %s.\n", source); return 1; }

	for(i=0; i<Nt; i++)
	{
		oldstr = line; newstr = NULL;
		if(!fgets(line, MAX_LINE, file)){ fprintf(stderr, "Can't read line.\n"); return 1; }
		T[i*4 + 0] = (int)strtol(oldstr, &newstr, 10); oldstr = newstr;
		T[i*4 + 1] = (int)strtol(oldstr, &newstr, 10); oldstr = newstr;
		T[i*4 + 2] = (int)strtol(oldstr, &newstr, 10); oldstr = newstr;
		T[i*4 + 3] = (int)strtol(oldstr, &newstr, 10);
	}		

	fclose(file);

	return 0;
}

int WriteTetrahedron_MSH(char *source, int Nt, int *T)
{
	FILE *file = NULL;
	int i;

	file = fopen(source, "w");
	if(!file) { fprintf(stderr, "Can't create file %s.\n", source); return 1; }

	for(i=0; i<Nt; i++)
		fprintf(file, "%d %d %d %d\n", T[i*4 + 0], T[i*4 + 1], T[i*4 + 2], T[i*4 + 3]);	
	
	fclose(file);

	return 0;
}

int ReadTriangle_MSH(char *source, int Ntr, int *Tr, int *TrType)
{
	FILE *file = NULL;
	char line[MAX_LINE], *oldstr, *newstr;
	int i;

	file = fopen(source, "r");
	if(!file) { fprintf(stderr, "Can't open file %s.\n", source); return 1; }

	for(i=0; i<Ntr; i++)
	{
		oldstr = line; newstr = NULL;
		if(!fgets(line, MAX_LINE, file)){ fprintf(stderr, "Can't read line.\n"); return 1; }
		Tr[i*3 + 0] = (int)strtol(oldstr, &newstr, 10); oldstr = newstr;
		Tr[i*3 + 1] = (int)strtol(oldstr, &newstr, 10); oldstr = newstr;
		Tr[i*3 + 2] = (int)strtol(oldstr, &newstr, 10); oldstr = newstr;
		TrType[i]   = (int)strtol(oldstr, &newstr, 10);
	}
	
	fclose(file);
	
	return 0;
}

int WriteTriangle_MSH(char *source, int Ntr, int *Tr, int *TrType)
{
	FILE *file = NULL;
	int i;

	file = fopen(source, "w");
	if(!file) { fprintf(stderr, "Can't create file %s.\n", source); return 1; }

	for(i=0; i<Ntr; i++)
		fprintf(file, "%d %d %d %d\n", Tr[i*3 + 0], Tr[i*3 + 1], Tr[i*3 + 2], TrType[i]);
	
	fclose(file);
	
	return 0;
}

int ReadMesh_MSB(char *source, int *Np, int *Nt, int *Ntr)
{
	FILE *file = NULL;

	file = fopen(source, "r");
	if(!file) { fprintf(stderr, "Can't open file %s.\n", source); return 1; }

	if(fscanf(file, "%d %d %d", Np, Nt, Ntr) != 3){ fprintf(stderr, "Can't read from file.\n"); return 1; }

	fclose(file);

	return 0;
}

int WriteMesh_MSB(char *source, int Np, int Nt, int Ntr)
{
	FILE *file = NULL;

	file = fopen(source, "w");
	if(!file) { fprintf(stderr, "Can't create file %s.\n", source); return 1; }

	fprintf(file, "%d\n%d\n%d\n", Np, Nt, Ntr);

	fclose(file);

	return 0;
}

int ReadCoordinate_MSB(char *source, int Np, double *C)
{
	FILE *file = NULL;
	int get;

	file = fopen(source, "rb");
	if(!file) { fprintf(stderr, "Can't open file %s.\n", source); return 1; }

	get = fread((void *)C, sizeof(double), Np*3, file);
	if(get != Np*3) { fprintf(stderr, "Can't read data from file %s.\n", source); fclose(file); return 1; }

	fclose(file);

	return 0;
}

int WriteCoordinate_MSB(char *source, int Np, double *C)
{
	FILE *file = NULL;
	int get;

	file = fopen(source, "wb");
	if(!file) { fprintf(stderr, "Can't create file %s.\n", source); return 1; }

	get = fwrite((void *)C, sizeof(double), Np*3, file);
	if(get != Np*3) { fprintf(stderr, "Can't write data to file %s.\n", source); fclose(file); return 1; }

	fclose(file);

	return 0;
}

int ReadTetrahedron_MSB(char *source, int Nt, int *T)
{
	FILE *file = NULL;
	int get;

	file = fopen(source, "rb");
	if(!file) { fprintf(stderr, "Can't open file %s.\n", source); return 1; }

	get = fread((void *)T, sizeof(int), Nt*4, file);
	if(get != Nt*4) { fprintf(stderr, "Can't read data from file %s.\n", source); fclose(file); return 1; }

	fclose(file);

	return 0;
}

int WriteTetrahedron_MSB(char *source, int Nt, int *T)
{
	FILE *file = NULL;
	int get;

	file = fopen(source, "wb");
	if(!file) { fprintf(stderr, "Can't create file %s.\n", source); return 1; }

	get = fwrite((void *)T, sizeof(int), Nt*4, file);
	if(get != Nt*4) { fprintf(stderr, "Can't write data to file %s.\n", source); fclose(file); return 1; }

	fclose(file);

	return 0;
}

int ReadTriangle_MSB(char *source, int Ntr, int *Tr, int *TrType)
{
	FILE *file = NULL;
	int i, get;

	file = fopen(source, "rb");
	if(!file) { fprintf(stderr, "Can't open file %s.\n", source); return 1; }

	for(i=0; i<Ntr; i++)
	{
		get = fread((void *)(&(Tr[i*3])), sizeof(int), 3, file);
		if(get != 3) { fprintf(stderr, "Can't read data from file %s.\n", source); fclose(file); return 1; }
		get = fread((void *)(&(TrType[i])), sizeof(int), 1, file);
		if(get != 1) { fprintf(stderr, "Can't read data from file %s.\n", source); fclose(file); return 1; }
	}
	
	fclose(file);
	
	return 0;
}

int WriteTriangle_MSB(char *source, int Ntr, int *Tr, int *TrType)
{
	FILE *file = NULL;
	int i, get;

	file = fopen(source, "wb");
	if(!file) { fprintf(stderr, "Can't create file %s.\n", source); return 1; }

	for(i=0; i<Ntr; i++)
	{
		get = fwrite((void *)(&(Tr[i*3])), sizeof(int), 3, file);
		if(get != 3) { fprintf(stderr, "Can't write data to file %s.\n", source); fclose(file); return 1; }
		get = fwrite((void *)(&(TrType[i])), sizeof(int), 1, file);
		if(get != 1) { fprintf(stderr, "Can't write data to file %s.\n", source); fclose(file); return 1; }
	}
	
	fclose(file);
	
	return 0;
}

#undef MAX_LINE

