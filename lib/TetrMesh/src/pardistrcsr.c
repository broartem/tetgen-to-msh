#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mpi.h>

#include "pardistrcsr.h"

static int Prank; // идентификатор процессора
static int Psize; // число процессоров

// ВСПОМОГАТЕЛЬНЫЕ ПОДПРОГРАММЫ
// СРАВНЕНИЕ ДВУХ РЕБЕР
static int CompareEdges(int *a, int *b);
// СОРТИРОВКА МАССИВА РЕБЕР
static void SortEdges(int *A, int l, int r);
// ВСПОМОГАТЕЛЬНЫЕ ПОДПРОГРАММЫ

int DistributedCSRGraphTetrahedron(int *vtxdist, int *tetdist, int *T, int **xadj, int **adjncy, MPI_Comm comm)
{
	int Np; // число узлов процессора
	int Nt; // число тетраэдров процессора
	int *Edges, Ne; // число ребер и ребра тетраэдров локальной области процессора
	int *sendlist = NULL; // таблица рассылки данных процессорам
	int i, j;
	double lstart, lfinish;
	
	MPI_Barrier(comm);
	
	MPI_Comm_size( comm, &Psize );
	MPI_Comm_rank( comm, &Prank );
	
	MPI_Barrier(comm);

	// число узлов и тетраэдров, описание которых находится в оперативной памяти процессора
	Np = vtxdist[Prank + 1] - vtxdist[Prank];
	Nt = tetdist[Prank + 1] - tetdist[Prank];

	MPI_Barrier(comm);

	Ne = Nt * 6 * 2;
	Edges = (int *)malloc(Ne * 2 * sizeof(int));
	if(!Edges) { fprintf(stderr, "[%03d]: DistributedCSRGraphTetrahedron -> ERROR!!! Can't allocate memory.\n", Prank); MPI_Abort(comm, 1); }

	for(i=0, j=0; i<Nt; i++)
	{
		// 0 - 1 | 0 - 2 | 0 - 3
		// 1 - 0 | 1 - 2 | 1 - 3
		// 2 - 0 | 2 - 1 | 2 - 3
		// 3 - 0 | 3 - 1 | 3 - 2
		
		Edges[j*2 + 0] = T[i*4 + 0]; Edges[j*2 + 1] = T[i*4 + 1]; j += 1;
		Edges[j*2 + 0] = T[i*4 + 0]; Edges[j*2 + 1] = T[i*4 + 2]; j += 1;
		Edges[j*2 + 0] = T[i*4 + 0]; Edges[j*2 + 1] = T[i*4 + 3]; j += 1;

		Edges[j*2 + 0] = T[i*4 + 1]; Edges[j*2 + 1] = T[i*4 + 0]; j += 1;
		Edges[j*2 + 0] = T[i*4 + 1]; Edges[j*2 + 1] = T[i*4 + 2]; j += 1;
		Edges[j*2 + 0] = T[i*4 + 1]; Edges[j*2 + 1] = T[i*4 + 3]; j += 1;

		Edges[j*2 + 0] = T[i*4 + 2]; Edges[j*2 + 1] = T[i*4 + 0]; j += 1;
		Edges[j*2 + 0] = T[i*4 + 2]; Edges[j*2 + 1] = T[i*4 + 1]; j += 1;
		Edges[j*2 + 0] = T[i*4 + 2]; Edges[j*2 + 1] = T[i*4 + 3]; j += 1;

		Edges[j*2 + 0] = T[i*4 + 3]; Edges[j*2 + 1] = T[i*4 + 0]; j += 1;
		Edges[j*2 + 0] = T[i*4 + 3]; Edges[j*2 + 1] = T[i*4 + 1]; j += 1;
		Edges[j*2 + 0] = T[i*4 + 3]; Edges[j*2 + 1] = T[i*4 + 2]; j += 1;
	} // for i

	SortEdges(Edges, 0, Ne-1);

	for(i=1; i<Ne; i++)
	{
		if( (Edges[i*2] == Edges[(i-1)*2]) && (Edges[i*2 + 1] == Edges[(i-1)*2 + 1]) )
			Edges[(i-1)*2] = (-1);
	} // for i

	for(i=0, j=0; i<Ne; i++)
	{
		if(Edges[i*2] != (-1))
		{
			Edges[j*2] = Edges[i*2];
			Edges[j*2 + 1] = Edges[i*2 + 1];
			j += 1;
		} // if
	} // for i

	Ne = j;
	{
		int *tmp = NULL;
		tmp = (int *)realloc(Edges, Ne * 2 * sizeof(int));
		if(tmp == NULL) { fprintf(stderr, "[%03d]: DistributedCSRGraphTetrahedron -> ERROR!!! Can't reallocate memory.\n", Prank); MPI_Abort(comm, 1); }
		Edges = tmp;
	}

	MPI_Barrier(comm);
	if(Prank == 0) fprintf(stderr, "[%03d]: DistributedCSRGraphTetrahedron -> STEP 1 = OK. Ne = %d\n", Prank, Ne);
	MPI_Barrier(comm);

	sendlist = (int *)malloc(Psize * 2 * sizeof(int));
	if(!sendlist) { fprintf(stderr, "[%03d]: DistributedCSRGraphTetrahedron -> ERROR!!! Can't allocate memory.\n", Prank); MPI_Abort(comm, 1); }
	memset(sendlist,  0, Psize * 2 * sizeof(int));

	for(i=0, j=0; ((i<Psize)&&(j<Ne)); i++)
	{
		if( (Edges[j*2] >= vtxdist[i]) && (Edges[j*2] < vtxdist[i+1]) )
		{
			sendlist[i*2] = j;
			while( (Edges[j*2] < vtxdist[i+1]) && (j < Ne) )
			{
				sendlist[i*2 + 1] += 1;
				j += 1;
			}
		} // if
	} // for i

	if(j != Ne) { fprintf(stderr, "[%03d]: DistributedCSRGraphTetrahedron -> ERROR!!! Wrong sendlist (check by j).\n", Prank); MPI_Abort(comm, 1); }
	for(i=0, j=0; i<Psize; i++) j += sendlist[i*2 + 1];
	if(j != Ne) { fprintf(stderr, "[%03d]: DistributedCSRGraphTetrahedron -> ERROR!!! Wrong sendlist (check summ).\n", Prank); MPI_Abort(comm, 1); }

	MPI_Barrier(comm);
	if(Prank == 0) fprintf(stderr, "[%03d]: DistributedCSRGraphTetrahedron -> STEP 2 = OK. Correct sendlist.\n", Prank);
	MPI_Barrier(comm);
	lstart = MPI_Wtime();

	{
		int *xxadj = NULL, *xadjncy = NULL, xNe = 0;
		MPI_Status status;
		int r[8192]; // 2007 !!!!!

		for(i=0; i<Psize; i++)
		{
			if(Prank == i)
			{
				int pos = 0, length;

				// 2007 ========================================================================================================== 
#if 0
				MPI_Allreduce(&sendlist[i*2 + 1], &xNe, 1, MPI_INT, MPI_SUM, comm);
#else
				{
					int ii;
					r[Prank] = sendlist[i*2 + 1];
					for(ii=0; ii<Psize; ii++) if(ii != i) MPI_Recv(&r[ii], 1, MPI_INT, ii, 1892, comm, &status);
					xNe = 0; for(ii=0; ii<Psize; ii++) xNe += r[ii];
				}
#endif
				// 2007 ==========================================================================================================

				xadjncy = (int *)malloc(xNe * 2 * sizeof(int));
				if(!xadjncy) { fprintf(stderr, "[%03d]: DistributedCSRGraphTetrahedron -> ERROR!!! Can't allocate memory.\n", Prank); MPI_Abort(comm, 1); }

				if(sendlist[i*2 + 1] != 0) { memcpy(xadjncy, &Edges[sendlist[i*2]*2], sendlist[i*2 + 1] * 2 * sizeof(int)); pos = sendlist[i*2 + 1]; }

				for(j=0; j<Psize; j++)
				{
					if(j != i)
					{
						// MPI_Recv(&length, 1, MPI_INT, j, 0, comm, &status); // 2007 !!!!!
						length = r[j]; // 2007 !!!!!

						if(length != 0)
						{
							MPI_Recv(&xadjncy[pos*2], length*2, MPI_INT, j, 0, comm, &status);
							pos += length;
						}
					} // if
				} // for j

				if(pos != xNe) { fprintf(stderr, "[%03d]: DistributedCSRGraphTetrahedron -> ERROR!!! Get wrong number of edges from processors.\n", Prank); MPI_Abort(comm, 1); }			
				fprintf(stderr, "[%03d]: DistributedCSRGraphTetrahedron -> STEP 3 = OK. xNe = %d\n", Prank, xNe);
			}

			if(Prank != i)
			{
				// 2007 ==========================================================================================================
#if 0
				MPI_Allreduce(&sendlist[i*2 + 1], &j, 1, MPI_INT, MPI_SUM, comm);
				MPI_Send(&sendlist[i*2 + 1], 1, MPI_INT, i, 0, comm);
#else
				MPI_Send(&sendlist[i*2 + 1], 1, MPI_INT, i, 1892, comm);
#endif
				// 2007 ==========================================================================================================

				if(sendlist[i*2 + 1] != 0) MPI_Send(&Edges[sendlist[i*2]*2], sendlist[i*2 + 1] * 2, MPI_INT, i, 0, comm);
			}
		} // for i - цикл по процессорам

		MPI_Barrier(comm);
		lfinish = MPI_Wtime();
		if(Prank == 0) fprintf(stderr, "^^^^^ PARALLEL DISTR = %.2lf\n", lfinish - lstart);
		MPI_Barrier(comm);

		// fprintf(stderr, "[%03d]: DistributedCSRGraphTetrahedron -> START STEP 4.\n", Prank);

		free(Edges);

		SortEdges(xadjncy, 0, xNe-1);

		MPI_Barrier(comm);
		if(Prank == 0) fprintf(stderr, "[%03d]: DistributedCSRGraphTetrahedron -> SORT = OK\n", Prank);
		MPI_Barrier(comm);

		for(i=1; i<xNe; i++)
		{
			if( (xadjncy[i*2] == xadjncy[(i-1)*2]) && (xadjncy[i*2 + 1] == xadjncy[(i-1)*2 + 1]) )
				xadjncy[(i-1)*2] = (-1);
		} // for i

		MPI_Barrier(comm);
		if(Prank == 0) fprintf(stderr, "[%03d]: DistributedCSRGraphTetrahedron -> DELETE SAME = OK\n", Prank);
		MPI_Barrier(comm);

		for(i=0, j=0; i<xNe; i++)
		{
			if(xadjncy[i*2] != (-1))
			{
				xadjncy[j*2] = xadjncy[i*2];
				xadjncy[j*2 + 1] = xadjncy[i*2 + 1];
				j += 1;
			} // if
		} // for i

		xNe = j;

		MPI_Barrier(comm);
		if(Prank == 0) fprintf(stderr, "[%03d]: DistributedCSRGraphTetrahedron -> REWRITE = OK xNe = %d\n", Prank, xNe);
		MPI_Barrier(comm);

		xxadj = (int *)malloc((Np + 1) * sizeof(int));
		if(!xxadj) { fprintf(stderr, "[%03d]: DistributedCSRGraphTetrahedron -> ERROR!!! Can't allocate memory.\n", Prank); MPI_Abort(comm, 1); }
		memset(xxadj,  0, (Np + 1) * sizeof(int));

		for(i=0; i<xNe; i++) xxadj[xadjncy[i*2] + 1 - vtxdist[Prank]] += 1;
		for(i=1; i<=Np; i++) xxadj[i] += xxadj[i-1];

		if(xxadj[Np] != xNe) { fprintf(stderr, "[%03d]: DistributedCSRGraphTetrahedron -> ERROR!!! Wrong xxadj array.\n", Prank); MPI_Abort(comm, 1); }

		for(i=0; i<xNe; i++) xadjncy[i] = xadjncy[i*2 + 1];

		{
			int *tmp = NULL;
			tmp = (int *)realloc(xadjncy, xNe * sizeof(int));
			if(tmp == NULL) { fprintf(stderr, "[%03d]: DistributedCSRGraphTetrahedron -> ERROR!!! Can't reallocate memory.\n", Prank); MPI_Abort(comm, 1); }
			xadjncy = tmp;
		}

		(*xadj) = xxadj;
		(*adjncy) = xadjncy;
	}

	MPI_Barrier(comm);
	if(Prank == 0) fprintf(stderr, "### FINISH PROCEDURE ###\n");
	MPI_Barrier(comm);

	return 0;
}

// ================================================================================================================================================

// ВСПОМОГАТЕЛЬНЫЕ ПОДПРОГРАММЫ
// 0 --> a == b
// 1 --> a > b
// (-1) --> a < b
static int CompareEdges(int *a, int *b)
{
	if(a[0] > b[0]) return 1;
	if(a[0] < b[0]) return (-1);

	if(a[1] > b[1]) return 1;
	if(a[1] < b[1]) return (-1);	

	return 0;
}

static void SortEdges(int *A, int l, int r)
{
  int tmp[2];
  int B[2] = { A[2*((l+r)/2)+0], A[2*((l+r)/2)+1] };  
  int i = l, j = r;

  while( i <= j )
  {
 	while( CompareEdges(&A[i*2], B) == (-1) ) i++;
    while( CompareEdges(&A[j*2], B) == 1 ) j--;
    if( i <= j )
    {
      tmp[0] = A[i*2+0]; tmp[1] = A[i*2+1];
      A[i*2+0] = A[j*2+0]; A[i*2+1] = A[j*2+1];
      A[j*2+0] = tmp[0]; A[j*2+1] = tmp[1];
      i++;
      j--;      
    }   
  }
  
  if( l < j ) SortEdges(A, l, j);
  if( i < r ) SortEdges(A, i, r);
}
// ВСПОМОГАТЕЛЬНЫЕ ПОДПРОГРАММЫ

// ================================================================================================================================================
