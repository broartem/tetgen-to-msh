#ifndef PARDISTRCSR_DEFINED
#define PARDISTRCSR_DEFINED

int DistributedCSRGraphTetrahedron(int *vtxdist, int *tetdist, int *T, int **xadj, int **adjncy, MPI_Comm comm);

#endif
