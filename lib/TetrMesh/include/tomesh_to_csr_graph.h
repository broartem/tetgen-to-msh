#ifndef TOMESHTOSCRGRAPH_DEFINED
#define TOMESHTOSCRGRAPH_DEFINED

int TOmesh_to_CSRGraph(int Np, int Nt, int **T, int No, int **O, int **xadj, int **adjncy, int lite);

#endif
